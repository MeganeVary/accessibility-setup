let buttonMenuAccessible = document.getElementById("ButtonMenuAccessible");
let containerMenu = document.getElementById("menuAccessible");
buttonMenuAccessible.addEventListener("click", () => {
  if (containerMenu.style.visibility == "hidden") {
    containerMenu.style.visibility = "visible";
  } else {
    containerMenu.style.visibility = "hidden";
  }
});

let closeMenuaccessible = document.getElementById("closeMenu");
closeMenuaccessible.addEventListener("click", () => {
  containerMenu.style.visibility = "hidden";
});

let button = document.getElementById("changeDyslexique");
button.addEventListener("click", () => {
  if (!document.body.classList.contains("accessDyslexique")) {
    document.body.classList.add("accessDyslexique");
  } else {
    document.body.classList.remove("accessDyslexique");
  }
});

let buttonPoliceDefault = document.getElementById("changePoliceDefault");
buttonPoliceDefault.addEventListener("click", () => {
  document.body.classList.remove("accessDyslexique");
});

let buttonFontSizeIncrease = document.getElementById("increaseFont");
buttonFontSizeIncrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("font-size");
  var fontSize = parseFloat(style);
  document.body.style.fontSize = fontSize + 5 + "px";
});

let buttonFontsizeDecrease = document.getElementById("decreaseFont");
buttonFontsizeDecrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("font-size");
  var fontSize = parseFloat(style);
  document.body.style.fontSize = fontSize - 5+ "px";
});

// pb quand la valeur revient à "normal"
let buttonFontLetterSpacingIncrease = document.getElementById("increaseSpace");
document.body.style.letterSpacing = +5 + "px";

buttonFontLetterSpacingIncrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("letter-spacing");
  var fontSize = parseFloat(style);
  document.body.style.letterSpacing = fontSize + 5 + "px";
});

let buttonFontLetterSpacingDecrease = document.getElementById("decreaseSpace");
document.body.style.letterSpacing = +1 + "px";
buttonFontLetterSpacingDecrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("letter-spacing");
  var fontSize = parseFloat(style);
  if (fontSize == 1) {
    console.log(fontSize);
  } else document.body.style.letterSpacing = fontSize - 5 + "px";
});

//

// line height
let buttonFontLineHeightIncrease = document.getElementById(
  "incrementLineHeight"
);
buttonFontLineHeightIncrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("line-height");
  var fontSize = parseFloat(style);
  console.log(fontSize);
  document.body.style.lineHeight = fontSize + 5 + "px";
  // pb si taille prédéfinis en css
});

let buttonFontLineHeightDecrease = document.getElementById(
  "decrementLineHeight"
);
buttonFontLineHeightDecrease.addEventListener("click", () => {
  var elBody = document.body;
  var style = window
    .getComputedStyle(elBody, null)
    .getPropertyValue("line-height");
  var fontSize = parseFloat(style);
  console.log(fontSize);
  document.body.style.lineHeight = fontSize - 5 + "px";
});

// surligner elements

// text aligns
let buttonTextAlignDefault = document.getElementById("changeTextAlignDefault");
buttonTextAlignDefault.addEventListener("click", () => {
  let elementsText = document.querySelectorAll(
    "header, main, section, div, footer, ul, li, nav, h1, h2, h3, h4, h5, h6, a, p, body"
  );
  var index = 0,
    length = elementsText.length;

  for (; index < length; index++) {
    if ((
      elementsText[index].classList.contains("textAlignLeftActive") ||
      elementsText[index].classList.contains("textAlignRightActive") ||
      elementsText[index].classList.contains("textAlignCenterActive"))
    )
      elementsText[index].classList.remove("textAlignLeftActive");
      elementsText[index].classList.remove("textAlignRightActive");
      elementsText[index].classList.remove("textAlignCenterActive");
  }
});

let buttontextAlignLeft = document.getElementById("textAlignLeft");
buttontextAlignLeft.addEventListener("click", () => {
  let elementsText = document.querySelectorAll(
    "header, main, section, div, footer, ul, li, nav, h1, h2, h3, h4, h5, h6, a, p, body"
  );
  var index = 0,
    length = elementsText.length;

  for (; index < length; index++) {
    if (!elementsText[index].classList.contains("textAlignLeftActive")) {
      elementsText[index].classList.add("textAlignLeftActive");
      elementsText[index].classList.remove("textAlignRightActive");
      elementsText[index].classList.remove("textAlignCenterActive");
    }
  }
});
let buttontextAlignRight = document.getElementById("textAlignRight");
buttontextAlignRight.addEventListener("click", () => {
  let elementsText = document.querySelectorAll(
    "header, main, section, div, footer, ul, li, nav, h1, h2, h3, h4, h5, h6, a, p, body"
  );
  var index = 0,
    length = elementsText.length;

  for (; index < length; index++) {
    if (!elementsText[index].classList.contains("textAlignRightActive")) {
      elementsText[index].classList.add("textAlignRightActive");
      elementsText[index].classList.remove("textAlignLeftActive");
      elementsText[index].classList.remove("textAlignCenterActive");
    } else {
      elementsText[index].classList.remove("textAlignRightActive");
    }
  }
});
let buttontextAlignCenter = document.getElementById("textAlignCenter");
buttontextAlignCenter.addEventListener("click", () => {
  let elementsText = document.querySelectorAll(
    "header, main, section, div, footer, ul, li, nav, h1, h2, h3, h4, h5, h6, a, p, body"
  );
  var index = 0,
    length = elementsText.length;

  for (; index < length; index++) {
    if (!elementsText[index].classList.contains("textAlignCenterActive")) {
      elementsText[index].classList.add("textAlignCenterActive");
      elementsText[index].classList.remove("textAlignLeftActive");
      elementsText[index].classList.remove("textAlignRightActive");
    } else {
      elementsText[index].classList.remove("textAlignCenterActive");
    }
  }
});


// la saturation

let buttonSaturationMono = document.getElementById("Monochrome");
buttonSaturationMono.addEventListener("click", () => {
  document.body.style.filter = "saturate" + "(0)";
});

let buttonSaturation50 = document.getElementById("color50");
buttonSaturation50.addEventListener("click", () => {
  document.body.style.filter = "saturate" + "(50%)";
});
let buttonSaturationDefault = document.getElementById("colorDefault");
buttonSaturationDefault.addEventListener("click", () => {
  document.body.style.filter = "saturate" + "()";
});

// cursor size et modelele

// Highlight links
let buttonLinks = document.getElementById("highlightLinks");
buttonLinks.addEventListener("click", () => {
  let links = document.getElementsByTagName("a");
  var index = 0,
    length = links.length;
  for (; index < length; index++) {
    // links[index].style.border = '2px solid #ff930d9c'
    if (!links[index].classList.contains("linkshighlightActive")) {
      links[index].classList.add("linkshighlightActive");
    } else {
      links[index].classList.remove("linkshighlightActive");
    }
  }
});

// Highlight tittle

let buttonTittles = document.getElementById("highlightTittles");
buttonTittles.addEventListener("click", () => {
  let tittles = document.querySelectorAll("h1, h2, h3, h4, h5, h6");
  var index = 0,
    length = tittles.length;

  for (; index < length; index++) {
    if (!tittles[index].classList.contains("tittlehighlightActive")) {
      tittles[index].classList.add("tittlehighlightActive");
    } else {
      tittles[index].classList.remove("tittlehighlightActive");
    }
  }
});

// sans images
let buttonPictures = document.getElementById("deletePictures");
buttonPictures.addEventListener("click", () => {
  let imgBalise = document.querySelectorAll("img");
  var index = 0,
    length = imgBalise.length;

  for (; index < length; index++) {
    if (!imgBalise[index].classList.contains("imgDisplaytActive")) {
      imgBalise[index].classList.add("imgDisplaytActive");
    } else {
      imgBalise[index].classList.remove("imgDisplaytActive");
    }
  }
});


// sans animation

// masque de lecture

// reading helper

// mute sounds

// profil

// dyslexique
// sourd
// aveugle
// sensible lumiere
